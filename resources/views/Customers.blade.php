
<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TESTE</title>

        <!-- Fonts -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.js"></script>


    </head>
    <body>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <form method="POST" action="/Customers" id="form" name="form">
              {!! csrf_field() !!}
              <input type="hidden" name="id" id="id" value="{{$customer->id}}">
              <input type="hidden" name="revenue" id="revenue" value="{{$customer->revenue}}">
              <div class="form-group">
                <label for="customerID">CustomerID</label>
                <input class="form-control" disabled name="customerID" id="customerID" placeholder="CustomerID" value="{{$customer->id}}">
              </div>
              <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" name="name" id="name" placeholder="Name" value="{{$customer->name}}">
              </div>
              <div class="form-group">
                <label for="since">Since (yyyy-mm-dd)</label>
                <input class="form-control" name="since" id="since" placeholder="Since" value="{{$customer->since}}">
              </div>
              <div class="form-group">
                <label for="revenue">Revenue</label>
                <input class="form-control" disabled name="revenue_show" id="revenue_show" placeholder="Revenue" value="{{$customer->revenue}}">
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
              <a class="btn btn-default" href="/" role="button">Home</a>
            </form>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
    </body>
</html>

<script>
$( document ).ready(function() {
  $("form[name='form']").validate({
    rules: {
      customerID: "required",
      name: "required",
      since: 'required',
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>

<style>
  .error{
    color: red;
  }
</style>
