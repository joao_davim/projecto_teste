
<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TESTE</title>

        <!-- Fonts -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.js"></script>

    </head>
    <body>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <form method="POST" action="/ShowDiscountTotal" name="form" id="form">
              {!! csrf_field() !!}
              <div class="form-group">
                <label for="threshold">Threshold</label>
                <input class="form-control" name="threshold" id="threshold" placeholder="Threshold" value="{{$threshold}}">
              </div>
              <div class="form-group">
                <label for="percentage">Percentage</label>
                <input class="form-control" name="percentage" id="percentage" placeholder="Percentage" value="{{$percentage}}">
              </div>
              <div class="form-group">
                <label for="active">Active</label>
                <input id="active" name="active" type="checkbox" <?php if($active == 1) echo "checked='checked'"?>>
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
              <a class="btn btn-default" href="/" role="button">Home</a>
            </form>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
    </body>
</html>

<script>
$( document ).ready(function() {
  $("form[name='form']").validate({
    rules: {
      threshold: {
        required:true,
        number:true
      },
      percentage: {
        required:true,
        number:true
      },
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>


<style>
  .error{
    color: red;
  }
</style>
