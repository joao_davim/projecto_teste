<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TESTE</title>

        <!-- Fonts -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    </head>
    <body>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <p>List Discounts</p>

              <ul>
                <li><a href="/ShowDiscountTotal">Discount applied to list</a></li>
                <li><a href="/listDiscountXbyY">Discount X by Y</a></li>
                <li><a href="/listCategory">Discount percentage by category</a></li>
                <li><a href="/listProducts">List Products</a></li>
                <li><a href="/listCustomers">List Customers</a></li>
                <li><a href="/calculate">Calculate</a></li>
              </ul>

          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
    </body>
</html>
