
<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TESTE</title>

        <!-- Fonts -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    </head>
    <body>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <table>
              <thead>
                <tr>
                  <th class="id">ID</th>
                  <th class="description">Descrição</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($products as $product)
                  <tr>
                    <td>
                      {{ $product->id }}
                    </td>
                    <td>
                      <a href="/Products/{{$product->id}}">{{ $product->description }}</a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            {{ $products->links() }}

            <a class="btn btn-default" href="/Products" role="button">Adicionar novo</a>
            <a class="btn btn-default" href="/" role="button">Home</a>

          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
    </body>
</html>

<style>

  table{
    margin-top:5%;
    margin-bottom: 5%;
    border-collapse: collapse;
  }

  table, td, th, tr{
    border: 1px solid black;
  }


  .id{
    width:20%;
  }

  .description{
    width: 80%;
    min-width: 700px;
  }


</style>
