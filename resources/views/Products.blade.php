
<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TESTE</title>

        <!-- Fonts -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.js"></script>

    </head>
    <body>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <form method="POST" action="/Products" id="form" name="form">
              {!! csrf_field() !!}
              <input type="hidden" name="id" id="id" value="{{$product->id}}">
              <div class="form-group">
                <label for="productID">ProductID</label>
                <input class="form-control" name="productID" id="productID" placeholder="ProductID" value="{{$product->productID}}">
              </div>
              <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control" name="description" id="description" placeholder="Description" value="{{$product->description}}">
              </div>
              <div class="form-group">
                <label for="price">Price</label>
                <input class="form-control" name="price" id="price" placeholder="Price" value="{{$product->price}}">
              </div>
              <div class="form-group">
                <label for="category">Category</label>
                <input class="form-control" name="category" id="category" placeholder="Category" value="{{$product->category}}">
              </div>
              <button type="submit" class="btn btn-default">Submit</button>
              <a class="btn btn-default" href="/" role="button">Home</a>
            </form>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>
    </body>
</html>

<script>
$( document ).ready(function() {
  $("form[name='form']").validate({
    rules: {
      productID: {
        required:true,
      },
      price: {
        required:true,
        number:true
      },
      category: {
        required:true,
        number:true
      },
      description: "required",
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>


<style>
  .error{
    color: red;
  }
</style>
