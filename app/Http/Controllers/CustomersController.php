<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customers;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class CustomersController extends Controller
{

    public function listCustomers()
    {
      try
      {
        $customers = Customers::paginate(10);
      }catch(ModelNotFoundException $e){
        $customers = null;
      }
      return view('listCustomers', ['customers' => $customers]);
    }

    public function Customers($id = null)
    {

      if($id===null){
        $customer = new Customers();
        $customer->revenue = 0;
        if(isset(Input::all()['id'])){
          $id = Input::all()['id'];
        }
      }

      if($id!==null){
        try
        {
          $customer = Customers::findOrFail($id);
        }catch(ModelNotFoundException $e){
          $customer = new Customers();
          $customer->revenue = 0;
        }
      }

      if(Input::all()){
        $customer->name = Input::all()['name'];
        $customer->since = Input::all()['since'];
        $customer->revenue = Input::all()['revenue'];

        $customer->save();
        return redirect('/listCustomers');
      }

      return view('Customers',['customer' => $customer]);
    }

}
