<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class ProductsController extends Controller
{

    public function listProducts()
    {
      try
      {
        $products = Products::paginate(10);
      }catch(ModelNotFoundException $e){
        $products = null;
      }
      return view('listProducts', ['products' => $products]);
    }

    public function Products($id = null)
    {

      if($id===null){
        $product = new Products();
        if(isset(Input::all()['id'])){
          $id = Input::all()['id'];
        }
      }

      if($id!==null){
        try
        {
          $product = Products::findOrFail($id);
        }catch(ModelNotFoundException $e){
          $product = new Products();
        }
      }

      if(Input::all()){
        $product->productID = Input::all()['productID'];
        $product->price = Input::all()['price'];
        $product->description = Input::all()['description'];
        $product->category = Input::all()['category'];

        $product->save();
        return redirect('/listProducts');
      }

      return view('Products',['product' => $product]);
    }

}
