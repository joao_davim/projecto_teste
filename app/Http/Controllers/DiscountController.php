<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DiscountTotal;
use App\DiscountXByY;
use App\DiscountCategory;
use App\Products;
use App\Customers;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class DiscountController extends Controller
{


    public function TotalDiscount()
    {
      /*
        When accessing this page, we try to read the record with the total discount from DB
        this record acts in a similar way to a Singleton, because it doesn't make sense to me to
        have more than one discound that depends and acts on the total value of the operation
      */
      try
      {
        $discount = DiscountTotal::firstOrFail();
      }catch(ModelNotFoundException $e){
        /*
          If the record is not in the DB we create a new instance of this Discount and initialize it
        */
        $discount = new DiscountTotal();
        $discount->threshold = 0;
        $discount->percentage = 0;
        $discount->active = 0;
      }

      /*
        If a the edit form is submited than we update the values, save the record and redirect the user to the HP
      */

      if(Input::all()){
        $discount->threshold = Input::all()['threshold'];
        $discount->percentage = Input::all()['percentage'];

        if(isset(Input::all()['active'])){
          $discount->active = 1;
        }else{
          $discount->active = 0;
        }

        $discount->save();
        return redirect('/');
      }

      /*
        If the form was NOT submited then we show the page with current values
      */

      return view('promotiontotal', ['threshold' => $discount->threshold, 'percentage' => $discount->percentage, 'active' => $discount->active]);
    }


    public function listXbyY()
    {

      /*
        We list this type of discounts 10 at a time, so we either fetch at least on record
        or we show an empty list (the layout suffers with the empty list)
      */

      try{
        $discounts = DiscountXbyY::paginate(10);
      }catch(ModelNotFoundException $e){
        $discounts = null;
      }
      return view('listXbyY', ['discounts' => $discounts]);
    }

    public function DiscountXbyY($id = null)
    {
      /*
        the presence of the optional $id parameter means that this is an operation over an existing record
      */
      if($id===null){
        /*
          so if it's null we create a new instance of this type of discount
        */
        $discount = new DiscountXbyY();
        /*
          however, if we receive the 'id' parameter from the form this is also an operation over an existing record
          and we save this id
        */
        if(isset(Input::all()['id'])){
          $id = Input::all()['id'];
        }
      }

      if($id!==null){
        /*
          if the id is not null then we try to fetch the corresponding record from the DB
        */
        try{
          $discount = DiscountXbyY::findOrFail($id);
        }catch(ModelNotFoundException $e){
          /*
            The fetch operation can fail, so we create a new instance of this discount
          */
          $discount = new DiscountXbyY();
        }
      }


      /*
        if the Input::all() is defined then the form was submited (instead of being simply a 'show' operation)
      */
      if(Input::all()){
        /*
          we update the discount instance, save it to DB and return to th HP
        */
        $discount->categoryID = Input::all()['categoryID'];
        $discount->description = Input::all()['description'];
        $discount->amount = Input::all()['amount'];
        $discount->offer = Input::all()['offer'];

        if(isset(Input::all()['active'])){
          $discount->active = 1;
        }else{
          $discount->active = 0;
        }
        $discount->save();
        return redirect('/listDiscountXbyY');
      }

      return view('DiscountXbyY',['discount' => $discount]);
    }


    public function listCategory()
    {
      /*
        We list this type of discounts 10 at a time, so we either fetch at least on record
        or we show an empty list (the layout suffers with the empty list)
      */
      try{
        $discounts = DiscountCategory::paginate(10);
      }catch(ModelNotFoundException $e){
        $discounts = null;
      }
      return view('listCategory', ['discounts' => $discounts]);
    }


    public function DiscountCategory($id = null)
    {
      /*
        the presence of the optional $id parameter means that this is an operation over an existing record
      */
      if($id===null){
        /*
          so if it's null we create a new instance of this type of discount
        */
        $discount = new DiscountCategory();
        if(isset(Input::all()['id'])){
          /*
            however, if we receive the 'id' parameter from the form this is also an operation over an existing record
            and we save this id
          */
          $id = Input::all()['id'];
        }
      }

      /*
        if this id is NOT null we fetch the discount from DB, or create a new on if we can't
      */
      if($id!==null){
        try{
          $discount = DiscountCategory::findOrFail($id);
        }catch(ModelNotFoundException $e){
          $discount = new DiscountCategory();
        }
      }

      /*
        if this function was called because the form was submited, we update the disocount instance, save it and return to DB
      */
      if(Input::all()){
        $discount->category = Input::all()['category'];
        $discount->description = Input::all()['description'];
        $discount->amount = Input::all()['amount'];
        $discount->percentage = Input::all()['percentage'];

        if(isset(Input::all()['active'])){
          $discount->active = 1;
        }else{
          $discount->active = 0;
        }
        $discount->save();
        return redirect('/listCategory');
      }

      return view('DiscountCategory',['discount' => $discount]);
    }


    public function Calculate(){

      /*
        We read the json from the defined URL
      */

      $url = env('SERVICE_URL');

      $categories = array();
      $json = json_decode(file_get_contents($url), true);

      /*
        For each item in the list
      */
      foreach ($json['items'] as $item_id => $list_entry){
        /*
          The product instance is fetched  so we can know its category that is not sent in the list
        */
        $productID = $list_entry['product-id'];
        try{
          $product = Products::where('productID',$productID)->firstOrFail();
        }catch(ModelNotFoundException $e){
          $product = null;
        }

        /*
          We see if there is any discount for this category
        */
        try{
          $discountXbyY = DiscountXbyY::where('categoryID',$product->category)->where('active',1)->firstOrFail();
        }catch(ModelNotFoundException $e){
          $discountXbyY = null;
        }
        if($discountXbyY){
          /*
            For this type of discount we check the number of items in the order against the number of items needed to
            get this discount
          */
          if($list_entry['quantity'] >= $discountXbyY->amount){
            /*
              If that condition is true we calculate the number of extra items the customer will carry
            */
            $extra_devices = floor($list_entry['quantity'] / $discountXbyY->amount);
            $json['items'][$item_id]['quantity'] = $list_entry['quantity'] + $extra_devices;

          }
        }

        /*
          Since we are already iterating through the array we can save the number of items per category and the cheapest one between them
        */
        if(!isset($categories[$product->category])){
          $categories[$product->category]['quantity'] = $list_entry['quantity'];
          $categories[$product->category]['cheapest'] = $list_entry['product-id'];
        }else{
          $categories[$product->category]['quantity'] += $list_entry['quantity'];
          if($list_entry['unit-price'] > $categories[$product->category]['cheapest']){
            $categories[$product->category]['cheapest'] = $list_entry['product-id'];
          }
        }
      }

      foreach ($categories as $categoryID => $category) {
        /*
          We see if there is any discount for this category
        */
        try{
          $DiscountCategory = DiscountCategory::where('category',$categoryID)->where('active',1)->firstOrFail();
        }catch(ModelNotFoundException $e){
          $DiscountCategory = null;
        }
        /*
          If there is a discount for this category
        */
        if($DiscountCategory){
          /*
            AND the quantity of items is larger than the quantity of items necessary to qualify for the discount
          */
          if($category['quantity'] > $DiscountCategory->amount){
            /*
              we get the ID of the cheapest item (from the array we constructed while iterating the shopping list)
            */
            $item_id = array_search($category['cheapest'], array_column($json['items'], 'product-id'));
            $percentage = $DiscountCategory->percentage / 100;
            /*
              We calculate the discount and update the shopping list
            */
            $json['items'][$item_id]['total'] = $json['items'][$item_id]['total'] - $percentage * $json['items'][$item_id]['unit-price'];
            $json['total'] = $json['total'] - $percentage * $json['items'][$item_id]['unit-price'];
          }
        }
      }

      /*
        We see if the discount applies to the entirity of the list is activa
      */
      try{
        $discount = DiscountTotal::where('active',1)->firstOrFail();
      }catch(ModelNotFoundException $e){
        $discount = null;
      }

      /*
        If it is then we calculate the discount
      */
      if($discount){
        $percentage = $discount->percentage / 100;
        $json['total'] = $json['total'] - $percentage * $json['total'];
      }

      /*
        before we return the updated shopping list we fetch the customer from DB and update the
        revenue we got from him/her/it
      */
      try{
        $customer = Customers::findOrFail($json['customer-id']);
      }catch(ModelNotFoundException $e){
        $customer = null;
      }

      if($customer){
        $customer->revenue += $json['total'];
        $customer->save();
      }

      return $json;
    }

}
