<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/ShowDiscountTotal', 'DiscountController@TotalDiscount');
Route::post('/ShowDiscountTotal', 'DiscountController@TotalDiscount');

Route::get('/listDiscountXbyY', 'DiscountController@listXbyY');
Route::get('/listCategory', 'DiscountController@listCategory');
Route::get('/listCustomers', 'CustomersController@listCustomers');
Route::get('/listProducts', 'ProductsController@listProducts');


Route::any('/Customers/{id?}', 'CustomersController@Customers');
Route::any('/Products/{id?}', 'ProductsController@Products');
Route::any('/DiscountXbyY/{id?}', 'DiscountController@DiscountXbyY');
Route::any('/DiscountCategory/{id?}', 'DiscountController@DiscountCategory');
//Route::post('/DiscountXbyY/{id?}', 'DiscountController@DiscountXbyY');


Route::any('/calculate', 'DiscountController@Calculate');
