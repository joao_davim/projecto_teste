<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('products', function (Blueprint $table) {
          $table->increments('id');
          $table->string('productID');
          $table->string('description');
          $table->integer('category');
          $table->float('price');
          $table->timestamps();
      });

      Schema::create('customers', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->date('since');
          $table->float('revenue');
          $table->timestamps();
      });

      Schema::create('promotion_total', function (Blueprint $table) {
          $table->increments('id');
          $table->float('threshold');
          $table->float('percentage');
          $table->boolean('active');
          $table->timestamps();
      });

      Schema::create('promotion_x_by_y', function (Blueprint $table) {
          $table->increments('id');
          $table->string('categoryID');
          $table->string('description');
          $table->integer('amount');
          $table->integer('offer');
          $table->boolean('active');
          $table->timestamps();
      });

      Schema::create('promotion_category', function (Blueprint $table) {
          $table->increments('id');
          $table->string('description');
          $table->string('category');
          $table->integer('amount');
          $table->float('percentage');
          $table->boolean('active');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('products');
        Schema::drop('customers');
        Schema::drop('promotion_total');
        Schema::drop('promotion_x_by_y');
        Schema::drop('promotion_category');
    }
}
