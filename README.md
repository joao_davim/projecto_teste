1- Copiar o ficheiro .env.example e chamar-lhe .env

2- Dentro do ficheiro .env substituir as crendenciais
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

pelas crendenciais para ligar à BD

3- No ficheiro .env criar a seguinte variável, e atribuir-lhe o URL onde deve ser lido o ficheiro .json com a lista de compras
SERVICE_URL = http://data.local/order3.json
